package com.github.michalbric;

import com.github.michalbric.core.network.Client;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.net.URL;

public class App extends Application{

    public static final Client CLIENT = new Client();
    public static Stage PRIMARY_STAGE;

    @Override
    public void start(Stage primaryStage) throws Exception{
        URL loginURI = getClass().getClassLoader().getResource("fxml/login.fxml");

        // fail fast if we cannot find the resource
        assert loginURI != null;

        Parent root = FXMLLoader.load(loginURI);
        PRIMARY_STAGE = primaryStage;
        PRIMARY_STAGE.getIcons().add(new Image("icon.png"));
        PRIMARY_STAGE.setTitle("AnonChat");
        PRIMARY_STAGE.setScene(new Scene(root));
        PRIMARY_STAGE.setResizable(false);
        PRIMARY_STAGE.setOnCloseRequest(e -> {
            Platform.exit();
            System.exit(0);
        });
        PRIMARY_STAGE.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
