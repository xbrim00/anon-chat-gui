package com.github.michalbric;

public enum WebSocketActionType {
    VERIFY,
    JOIN,
    LEAVE,
    MESSAGE
}
