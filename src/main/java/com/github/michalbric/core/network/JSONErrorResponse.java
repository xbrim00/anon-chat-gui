package com.github.michalbric.core.network;

import lombok.RequiredArgsConstructor;
import lombok.Value;

@Value
@RequiredArgsConstructor(staticName = "with")
class JSONErrorResponse {
    private final String reason;
}
