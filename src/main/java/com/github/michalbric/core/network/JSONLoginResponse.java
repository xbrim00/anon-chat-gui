package com.github.michalbric.core.network;

import lombok.RequiredArgsConstructor;
import lombok.Value;

@Value
@RequiredArgsConstructor(staticName = "of")
class JSONLoginResponse {
    private final String userName;
    private final String userID;
}
