package com.github.michalbric.core.network;

import com.github.michalbric.ChatMessage;
import com.github.michalbric.WebSocketActionType;
import com.github.michalbric.core.model.RoomDAO;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import okhttp3.*;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Optional;

public class Client extends WebSocketListener {

    private static final MediaType JSON_CONTENT_TYPE = MediaType.parse("application/json");
    private final static int REST_PORT = 8081;
    private final static int WS_PORT = 8091;
    private final static String SERVER_HOST = "localhost";
    private final static String LOGIN_URI_PART = "/login";
    private final static String LOGOUT_URI_PART = "/logout";
    private final static String FULL_LOGIN_URI = "http://" + SERVER_HOST + ":" + REST_PORT + LOGIN_URI_PART;
    private final static String FULL_LOGOUT_URI = "http://" + SERVER_HOST + ":" + REST_PORT + LOGOUT_URI_PART;
    private static final String LOGOUT_TEMPLATE = "{\"userID\":\"%s\"}";
    private final Request LOGIN_REQUEST = new Request.Builder().url(FULL_LOGIN_URI).build();

    private final static String WS_URI = "ws://" + SERVER_HOST + ":" + WS_PORT;

    private final OkHttpClient HTTP_CLIENT = new OkHttpClient();
    private final Gson GSON = new Gson();

    private UserInfo currentInfo;
    private WebSocket webSocket;


    public Optional<String> requestName() {
        if (currentInfo != null) {
            Request logoutReq = new Request.Builder()
                    .url(FULL_LOGOUT_URI)
                    .method("POST", RequestBody.create(JSON_CONTENT_TYPE, String.format(LOGOUT_TEMPLATE, currentInfo.getId())))
                    .build();
            try {
                HTTP_CLIENT.newCall(logoutReq).execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            currentInfo = null;
        }
        try(Response res = HTTP_CLIENT.newCall(LOGIN_REQUEST).execute()) {
            ResponseBody body = res.body();
            if (body == null) {
                return Optional.empty();
            }
            String bodyString = body.string();
            if (StringUtils.isEmpty(bodyString)) {
                return Optional.empty();
            }
            JSONLoginResponse jsonLoginResponse = GSON.fromJson(bodyString, JSONLoginResponse.class);
            currentInfo = UserInfo.of(jsonLoginResponse.getUserName(), jsonLoginResponse.getUserID());
            return Optional.of(jsonLoginResponse.getUserName());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public void connect() {
        Request wsReq = new Request.Builder()
                .url(WS_URI)
                .build();
        webSocket = HTTP_CLIENT.newWebSocket(wsReq, this);
    }

    public void join(final String roomName) {
        roomAction(WebSocketActionType.JOIN, roomName, null);
    }

    public void leave(final String roomName) {
        roomAction(WebSocketActionType.LEAVE, roomName, null);
    }

    public void sendMessage(final String roomName, final String text) {
        roomAction(WebSocketActionType.MESSAGE, roomName, text);
    }

    private void roomAction(final WebSocketActionType type, final String roomName, final String text) {
        WebSocketAction roomMsg = WebSocketAction.of(type, roomName, text, currentInfo.getId(), currentInfo.getUserName());
        String jsonPayload = GSON.toJson(roomMsg);
        webSocket.send(jsonPayload);
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
        WebSocketAction verifyAction = WebSocketAction.of(WebSocketActionType.VERIFY, null, null, currentInfo.getId(), currentInfo.getUserName());
        String jsonPayload = GSON.toJson(verifyAction);
        webSocket.send(jsonPayload);
    }

    @Override
    public void onClosing(WebSocket webSocket, int code, String reason) {
        System.out.println("huh?");
    }

    @Override
    public void onClosed(WebSocket webSocket, int code, String reason) {
        System.out.println("y tho");
    }

    @Override
    public void onMessage(WebSocket webSocket, String text) {
        try {
             ChatMessage wsAction = GSON.fromJson(text, ChatMessage.class);
            switch (wsAction.getType()) {
                case JOIN:
                    RoomDAO.addUser(wsAction.getRoomName(), wsAction.getAuthorName());
                    break;
                case LEAVE:
                    RoomDAO.removeUser(wsAction.getRoomName(), wsAction.getAuthorName());
                    break;
                case MESSAGE:
                    RoomDAO.addMessage(wsAction.getRoomName(), wsAction.getAuthorName(), wsAction.getText());
                    break;
                default:
                    System.out.println("Unknown WebSocketAction type");
            }
        } catch (JsonSyntaxException ex) {
            ex.printStackTrace();
        }
    }

    @Value
    @RequiredArgsConstructor(staticName = "of")
    private static class UserInfo {
        private final String userName;
        private final String id;
    }

    @Value
    @RequiredArgsConstructor(staticName = "of")
    private static class WebSocketAction {
        private final WebSocketActionType type;
        private final String target;
        private final String text;
        private final String userID;
        private final String userName;
    }
}
