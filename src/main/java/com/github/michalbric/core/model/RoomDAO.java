package com.github.michalbric.core.model;

import javafx.application.Platform;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class RoomDAO {

    private static Map<String, Room> rooms = new HashMap<>();
    private static final String MESSAGE_TEMAPLTE = "%s (%s):" + System.lineSeparator() + "%s";
    private static final DateTimeFormatter MESSAGE_TIMESTAMP_FORMATTER = DateTimeFormatter.ofPattern("H:m");

    public synchronized static Optional<Room> getRoom(final String name) {
        if (rooms.containsKey(name)) {
            return Optional.of(rooms.get(name));
        }
        return Optional.empty();
    }

    public synchronized static void addRoom(final String name) {
        if (rooms.containsKey(name)) {
            return;
        }
        rooms.put(name, Room.of(name));
    }

    public synchronized static void addUser(final String roomName, final String userName) {
        Platform.runLater(() -> {
            if (rooms.containsKey(roomName) && !rooms.get(roomName).getCurrentUsers().contains(userName)) {
                rooms.get(roomName).getCurrentUsers().add(userName);
                rooms.get(roomName).getCurrentUsers().sort(String::compareTo);
            }
        });
    }

    public synchronized static void removeUser(final String roomName, final String userName) {
        Platform.runLater(() -> {
            if (rooms.containsKey(roomName)) {
                rooms.get(roomName).getCurrentUsers().remove(userName);
            }
        });
    }

    public synchronized static void addMessage(final String roomName, final String author, final String text) {

        Platform.runLater(() -> {
            Optional<Room> optRoom = getRoom(roomName);
            if (!optRoom.isPresent()) {
                return;
            }
            Room targetRoom = optRoom.get();
            String newMessage = buildMessageString(author, text);
            targetRoom.getMessages().add(newMessage);
        });
    }

    private static String buildMessageString(final String author, final String text) {
        String timestamp = LocalDateTime.now().format(MESSAGE_TIMESTAMP_FORMATTER);
        return String.format(MESSAGE_TEMAPLTE, author, timestamp, text);
    }
}
