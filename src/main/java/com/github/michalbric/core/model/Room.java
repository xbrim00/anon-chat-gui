package com.github.michalbric.core.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.Data;

@Data
public class Room {
    private final String name;
    private final ObservableList<String> currentUsers;
    private final ObservableList<String> messages;

    private Room(final String name) {
        this.name = name;
        this.currentUsers = FXCollections.observableArrayList();
        this.messages = FXCollections.observableArrayList();
    }

    static Room of(final String name) {
        return new Room(name);
    }
}
