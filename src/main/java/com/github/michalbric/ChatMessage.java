package com.github.michalbric;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor(staticName = "of")
public class ChatMessage {
    private final WebSocketActionType type;
    private final String authorName;
    private final String roomName;
    private final String text;
}
