package com.github.michalbric.controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

import java.io.IOException;

public class SingleMessageView extends VBox {

    private static final String NAME_DATE_FORMAT = "%s (%s)";

    @FXML private Label lblNameDate;
    @FXML private Label textArea;

    public SingleMessageView() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource(
                "fxml/message.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    void fillData(final String name, final String time, final String text) {
        this.textArea.setText(text);
        this.textArea.setFont(Font.font ("Ubuntu", 20));
        //this.textArea.setPrefSize(200, 200);
        this.textArea.prefWidthProperty().bind(this.widthProperty());
        this.lblNameDate.setText(String.format(NAME_DATE_FORMAT, name, time));
    }
}
