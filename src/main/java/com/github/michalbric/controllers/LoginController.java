package com.github.michalbric.controllers;

import com.github.michalbric.App;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class
LoginController implements Initializable {

    private String currentUserName;

    @FXML
    private TextField txtFieldName;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        handleChangeNameClicked();
    }

    @FXML
    private void handleChangeNameClicked() {
        App.CLIENT.requestName().ifPresent(userName -> {
            txtFieldName.setText(userName);
            currentUserName = userName;
        });
    }

    @FXML
    private void handleJoinClicked() throws IOException {
        App.CLIENT.connect();
        URL mainWindowURI = getClass().getClassLoader().getResource("fxml/mainWindow.fxml");
        assert mainWindowURI != null;

        Parent root = FXMLLoader.load(mainWindowURI);
        App.PRIMARY_STAGE.setTitle("Anon Chat (" + currentUserName + ")");
        App.PRIMARY_STAGE.setScene(new Scene(root));

    }
}
