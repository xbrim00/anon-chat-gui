package com.github.michalbric.controllers;

import com.github.michalbric.App;
import com.github.michalbric.core.model.Room;
import com.github.michalbric.core.model.RoomDAO;
import javafx.application.Platform;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Callback;
import org.apache.commons.lang3.StringUtils;

import java.net.URL;
import java.util.HashSet;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;

public class MainController implements Initializable {
    @FXML private TabPane tabPaneRooms;
    @FXML private ListView<String> roomUsersListView;
    @FXML private TextField msgTextField;
    private String currentRoomSelection;
    private Set<String> connectedRooms = new HashSet<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ListView<String> messagesListView = new ListView<>();
        messagesListView.setCellFactory(messagesCellFactory(messagesListView));
        msgTextField.setOnKeyPressed(e -> {
            if (e.getCode().equals(KeyCode.ENTER)) {
                handleSendClicked();
            }
        });
    }

   // ui pulls from core.moddel and populates the list views with the messages..

    private Callback<ListView<String>, ListCell<String>> messagesCellFactory(final ListView<String> messagesListView) {
        return new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> list) {

                return new ListCell<String>() {
                    private Text text;

                    @Override
                    public void updateItem(String item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || empty) {
                            setGraphic(null);
                            return;
                        }
                        if (!isEmpty()) {
                            text = new Text(item);
                            text.setWrappingWidth(messagesListView.getWidth() - 20);
                            setGraphic(text);
                        }
                    }
                };
            }
        };
    }



    @FXML
    private void handleSendClicked() {
        String message = msgTextField.getText();
        if (StringUtils.isBlank(message)) {
            msgTextField.setText("");
            return;
        }
        App.CLIENT.sendMessage(currentRoomSelection, message.trim());
        msgTextField.setText("");

        /*ChatMessage newMsg = ChatMessage.of(WebSocketActionType.MESSAGE, "bob", "pokec", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut porttitor quam non orci sagittis convallis. Fusce finibus aliquet velit, a mattis arcu molestie at. Mauris vel porta erat, a convallis felis. Sed scelerisque lacus justo, vel interdum nulla dapibus eu. Vivamus at urna molestie, ornare sem id, egestas odio" + counter++);

         */
        //observableChatMessages.add("bob" + " (21:22)"+ ":" + System.lineSeparator() + "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut porttitor quam non orci sagittis convallis. Fusce finibus aliquet velit, a mattis arcu molestie at. Mauris vel porta erat, a convallis felis. Sed scelerisque lacus justo, vel interdum nulla dapibus eu. Vivamus at urna molestie, ornare sem id, egestas odio" + counter++);
        //Platform.runLater(() -> messageListView.scrollTo(observableChatMessages.size() - 1));

    }

    @FXML
    private void handleNewClicked() {
        TextInputDialog newRoomDialog = new TextInputDialog();
        newRoomDialog.setTitle("Join New Room");
        newRoomDialog.setContentText("Room Name");
        newRoomDialog.setHeaderText(null);
        Optional<String> optRoomName = newRoomDialog.showAndWait();
        optRoomName.ifPresent(roomName -> {
            if (connectedRooms.contains(roomName)) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setHeaderText("You're already in that room");
                alert.showAndWait();
                return;
            }
            connectedRooms.add(roomName);

            App.CLIENT.join(roomName);
            Tab newRoomTab = new Tab(roomName);
            RoomDAO.addRoom(roomName);
            newRoomTab.setOnSelectionChanged(event -> {
                Optional<Room> optRoom = RoomDAO.getRoom(roomName);
                currentRoomSelection = roomName;
                optRoom.ifPresent(room -> {
                    ListView<String> messageListView = new ListView<>();
                    ObservableList<String> observableMessages = room.getMessages();
                    messageListView.setItems(observableMessages);
                    observableMessages.addListener((ListChangeListener<String>) c -> Platform.runLater(() -> messageListView.scrollTo(observableMessages.size() - 1)));
                    newRoomTab.setContent(messageListView);
                    roomUsersListView.setItems(room.getCurrentUsers());
                });
            });
            newRoomTab.setOnClosed(e -> App.CLIENT.leave(roomName));
            tabPaneRooms.getTabs().add(newRoomTab);
            tabPaneRooms.getSelectionModel().select(newRoomTab);
        });
    }
}
